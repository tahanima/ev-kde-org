<?php
  $page_title = "Free Desktop Communities come together at the Gran Canaria Desktop Summit";
  $site_root = "../";
  include "header.inc";
?>
<p>
This year's Gran Canaria Desktop Summit represented the first time the GNOME and KDE communities have co-located their annual conferences in the same location. 852 free software advocates from 46 countries gathered together last week to discuss and enhance the free desktop experience at the first ever Gran Canaria Desktop Summit.
<p />
<em>"The Gran Canaria Desktop Summit was a milestone not only for the KDE and GNOME communities, but also for the free desktop in general."</em> said Cornelius Schumacher, president of KDE e.V., <em>"New collaboration efforts were started and existing ones revitalized. We already have seen results for example in the area of the semantic desktop, and on improving the specification processes on freedesktop.org. I'm sure we'll see more results in the near future."</em>
<p />
 
The summit accomplished its goal of increasing co-operation between GNOME and KDE to improve the Free Desktop experience. Throughout the conference there were many examples of  successful collaboration including shared technologies, community co-operation and growth of the local free software community
<p />
 
<em>"I was really excited to see all of the energy at the conference - 800 free desktop supporters in the same building!"</em> said Vincent Untz, Director and Chairman of the GNOME Board. <em>"I heard conversations about search technologies, recruiting developers and marketing. Both our communities benefited and I look forward to seeing the benefits passed on to GNOME users."</em>
 </p>

<h3>Shared Technology</h3>
<p>
KDE and GNOME benefit from shared technologies in multimedia, metadata storage, desktop search, application messaging and hardware integration. These shared technologies provide users with improved integration and a consistent user experience. Discussions during the summit resulted in agreements to continue to work on shared technologies, shared interfaces and shared code. In particular, several working sessions around the freedesktop.org initiative resulted in clearer processes for  for sharing specifications and technologies which will accelerate the ability of both projects and the greater free desktop community to collaborate and communicate with other projects.
 </p>

<h3>Community Co-operation</h3>
<p>
In the domains where KDE and GNOME share technology, global teams came together to work on more effective ways of collaborating. Members of both communities came together to discuss issues that affected desktop projects, from recruiting and maintaining bugsquad team members to free desktop marketing efforts to kernel technologies that affect both projects. Teams in areas as diverse as the bug squad, accessibility and multimedia teams shared experiences and knowledge, and resolved to work more closely together. The projects share many values like an interest in providing free accessibility to people around the world that can be accomplished better by working together.
</p>

<h3>Local Free Software Community</h3>
<p>
Co-locating GUADEC and Akademy resulted in a large number of key free software developers congregating in Gran Canaria. They attracted developers from related projects world wide and invigorated the local software community. GUADEC-es and Akademy-es took also place during the event, as an effort to increase the number of spanish developers involved. The devolpment of an introductory program in spanish for local students and developers allowed the a record number of local free software supporters showed up - over 300 free software supporters from the Canary Islands showed up to listen, participate and discuss free desktop issues with the GNOME and KDE communities. The Gran Canaria Desktop Summit was supported by the local free software community which includes strong GNOME and KDE presence through GNOME Hispano and KDE España, including support from The Cabildo of Gran Canaria, both local universities (ULPGC and ULL), ESLIC, ASOLIF, and GULIC, among others.
<p />
 
The Gran Canaria Desktop Summit was a success. The conversations that started at the summit will have an impact on the upcoming release of the GNOME and KDE desktops and will continue to foster cooperation between the GNOME and KDE communities. The communities are planning cross desktop hackfests to implement the ideas discussed at the summit.  Free desktop users can expect to see the results of this conference in upcoming releases of the GNOME and KDE desktops.
<p />
 
GNOME and KDE plan to join together again in future years to make sure they are working effectively together to share technologies and advance the free desktop. The cooperation and conversations that began between the KDE and GNOME communities will continue into the future and in events like hackfests throughout the year, but next year the conferences will be hosted separately. GUADEC and Akademy hope to see both GNOME and KDE developers at their events as their communities work more closely together on joint technologies.
<p />
 
Both projects believe that the co-location of the GNOME conference GUADEC and the KDE conference Akademy was a successful event that brought the two communities together and they look forward to more co-located events in the years to come.
 
<p />
For more information on the organizations mentioned in this press release:
<ul>
 <li>KDE e.V.: <a href="http://ev.kde.org/">ev.kde.org</a></li>
 <li>KDE España: <a href="http://www.kde-espana.es">kde-espana.es</a> 
 <li>GNOME Foundation: <a href="http://www.gnome.org/foundation">gnome.org/foundation</a></li>
 <li>GNOME Hispano: <a href="http://es.gnome.org">es.gnome.org</a></li>
 <li>Cabildo of Gran Canaria: <a href="http://www.grancanaria.com">grancanaria.com</a></li>
 <li>ULPGC: <a href="http://www.ulpgc.es">ulpgc.es</a></li>
 <li>ULL: <a href="http://www.osl.ull.es">osl.ull.es</a></li>
 <li>ESLIC: <a href="http://www.eslic.es/">eslic.es</a></li>
 <li>ASOLIF: <a href="http://www.asolif.org">asolif.org</a></li>
 <li>GULIC: <a href="http://www.gulic.org">gulic.org</a></li>
</ul>
</p>


<h2>Frequently Asked Questions</h2>

<p>
<strong>Why did you decide to co-locate in 2009?</strong><br />

The GNOME and KDE communities decided to co-locate Akademy and GUADEC so that they could build the free desktop community and cooperate on technologies that make sense for both desktops.
 </p>

<p>
<strong>Why did you decide not to co-locate in 2010?
</strong><br />
It's important to us to continue to build the GNOME and KDE brands. In particular, with the release of GNOME 3.0 coming soon, the GNOME community wants to make sure they can focus on that.
<br />
Both communities want to co-locate again and hope to bring even more free desktop communities.
</p>
<p>
<strong>Will you co-locate in the future?
</strong><br />
Yes. We are talking about co-locating events like hackfests now and plan to co-locate Akademy and GUADEC in a Desktop Summit in the future.
 </p>
<p>
<strong>Was the conference a success?
</strong><br />
Absolutely. The conference was a success for a community and technology perspective and was instrumental in furthering the free desktop movement.
</p>
<p>
<strong>You said the co-location was a success but you've decided not to do it again, why?
 </strong><br />
When we decided to co-locate GUADEC and Akademy we were unsure if it was something we'd do once or every year or every x number of years. We've decided it was a success and we'd like to do it again in the future, but not next year. It's important to us to continue to build the GNOME and KDE brands. In particular, with the release of GNOME 3.0 coming soon, the GNOME community wants to make sure they can focus on that.
<br />
We will co-locate again in the future.
</p>

<p>
<strong>
Where will GUADEC and Akademy be next year?
</strong><br />
We don't know yet but the call for bids will be going out this week.
</p>

<h3>About KDE:</h3>
<p>
KDE is an international technology team that creates free and open source 
software for desktop and portable computing. Among KDE's products are a modern 
desktop system for Linux and UNIX platforms, comprehensive office productivity 
and groupware suites and hundreds of software titles in many categories 
including Internet and web applications, multimedia, entertainment, 
educational, graphics and software development. KDE software is translated 
into more than 60 languages and is built with ease of use and modern 
accessibility principles in mind. KDE4's full-featured applications run 
natively on Linux, BSD, Solaris, Windows and Mac OS X. 
</p>

<?php
  include("footer.inc");
?>
