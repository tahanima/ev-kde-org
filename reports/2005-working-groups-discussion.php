<?php
$page_title = "KDE Working Groups Discussion";
include "header.inc";
?>

<h2>Summary</h2>

<p>There was an open meeting for 3 hours at aKademy 2005 discussing how to make working groups for KDE and allow e.V. to give KDE technical guidance, as agreed at the e.V. assembly on Friday Aug 26th.  The idea of the working groups is to create a structure which will formalise some roles within KDE and enhance coordination within KDE, communication between parts of KDE and delegation.  In doing so they will be able to make decisions and perform conflict resolution.  The meeting brainstormed a list of groups and brought it down to four groups with various responsibilities (show mind map).  The "non-technical administration" group was later dropped because it covered most areas that are the direct responsibility of the e.V. board.  There was some discussion about whether we should go with the groups that came out of the discussion following the brainstorm or groups that are self-forming, it was decided to go with the former.  There will be meetings a few times a year with 1 person from each working group and the e.V. board to report back on their areas of KDE.  Decisions will be made by the e.V. membership at the highest level, the e.V. board plus working group Representatives next and the working groups themselves below that (with the normal bazaar development below that).  Most decisions will be taken at the lowest levels, escalating as needed.  Three groups are now going to decide how to form each group, technical, marketing and human-computer interaction and proposals for each group will be brought back to the e.V. soon.</p>

<p>These notes are written in the third person because they often paraphrase rather than quoting directly.</p>

<p>Notes taken by Jonathan Riddell</p>

<hr />

<p>Mirko Boehm opens.  By now we have all figured out that KDE is not a project, because a project has a beginning and an end and we don't want KDE to have an end.  So we have to find a permanent structure that helps us create this desktop environment.  That's why he said we need this structure in KDE to give us a focus to find out answers to questions like "who to speak to if I want to find out about merchandise".  The board has some ideas but they don't want to tell us how to run.  Right now the board has more money than we can spend.  We don't have a plan for using our money, so we'd like to achieve that.  5 years ago we were a project of say 30 people, if you were lucky you met them at a conference so you could talk to 21 of them.  Today we have almost 1000 Subversion accounts, knowing all of them is hard.  We want a structure to focus our work, solve problems like marketing and technical.  Stephan Kulow has an idea to start us.</p>

<p>Stephan Kulow:  we not only decided that we want working groups but also that we want technical guidance from the e.V.  One of these working groups could be a technical working group.  What he would like to have as the current release coordinator is to spread his responsibilities to that working group, so if he can't make up his mind on something that's affecting a release he can ask a smaller group then the whole 200 subscribers to kde-core-devel.  He would like smaller groups, not sure what size, about 7, 8, 9.  Zack shouts out 7 is good.  Stephan: doesn't have to be private.  When he asks something on the kde-core-devel list many people ignore, some follow the arguments and say they agree, then there's one who disagrees.  The process does work but it's not efficient and people who disagree have a bad feeling.  He would like a council he can go to with questions.  </p>

<p>Cornelius Schumacher: you propose to replace release coordinator with release team?</p>

<p>Stephan: yes.  He has people helping him with the job but would like smaller team discussions about what features to approve, he has so many people saying I have this feature but it's not clear how unstable it will be or how much work for translators.  He can't judge for everything in KDE.</p>

<p>David Faure: it's good to have something to fall back on.  But there's cases where there's better groups to fall back on like multimedia or artists.</p>

<p>Stephan: example if Arts should be removed from KDE trunk, asked Scott Wheeler who said most people agreed but two people disagreed saying it would still be used for KDE 4.  So need a technical group with chair so can come to decision.</p>

<p>Mirko: this is one aspect of it, and we should collect a point of view on the idea.  </p>

<p>Konold: he wants to come back to the structural thing and not discussing 7 or 9 people on the groups.  It should be transparent so people not here can join in.  In the past when Matthias Ettrich discussed it we decided not to have this leadership thing.  If we are honest we discovered the need to structure in KDE, that's why we have different mailing lists for kde-multimedia separate from core-devel.  When an area gets more important we get a new mailing list for that.  This approach does not scale to large groups.  So we need more structure than the current peer to peer one.  One disadvantage of this is shouting, people who can shout and write e-mails faster get more of a say.  Decisions on the phone or irc are not really transparent.  If mailing lists are trying to come to consensus we have to come to coherency.  We did well in the past but we should start changing it.  He was opposed to working groups in the past but has seen things have changed and now supports it.  Doesn't support having closed mailing lists and secret meetings that let experts come up with a decision and tell developers what to do.  If Kulow needs a decision he asks and gets a feeling and slowly feels able to act.  If people ignore something that's OK if they don't feel com petant to make a decision.  Working groups are a good idea.  In the past a lot of things got lost and with working groups we can distribute and direct responsibility.  That can be done in an open manor like leaving the mailing lists open.  Doesn't mean everyone has to be able to comment on them.  For new developers this will make KDE more attractive, the old peer to peer model doesn't help new developers, he's got information from new coders that the current structure doesn't help them.  </p>

<p>Mirko: KDE is successful because we are open and free, a student can join the project and start to contribute.  This has got harder because the group has got so big.  We have to make decisions at some point. Coolo has to make decisions and every time someone asks him to make a decision as release coordinator he looses a hair and we can't go on like that.  We have to find a spot that keeps things open, a balance.</p>

<p>Konold: to keep the freedom we had in the past we have to find structure.  In the American frontier it started as free and you can go anywhere, as more people came in you need structure like roads.</p>

<p>Richard Moore: most decisions are still going to be made by maintainers of individual modules.  But for decisions that effect more areas we need working groups.  </p>

<p>Mirko: so you are saying any decision should be made at the lowest level possible, as soon as there is a conflict that should go higher.</p>

<p>Andras Mantia: maybe we can find people who are trusted like Scott in multimedia or Anne-Marie in Edu and get them to decide together if there is an issue.</p>

<p>Zack Rusin: let Aaron talk first, he looks more excited.</p>

<p>Aaron Seigo: hope it will be more than conflict resolution.  KDE is getting very big and we are trying to create an integrated environment, as it gets bigger, one of the challenges even though we have individual maintainers we don't have an effective way for the multimedia people to talk to the documentation people, we have little groups but not enough coordination between them.  While conflict resolution is a part of it he hopes it's also a coordination body that can bring all the pieces together to get this group talking to this other group to work well.  </p>

<p>Lauri Watts: you're basically describing how it works and has worked for 10 years, there's a core team that makes decisions, a release engineering team that makes decisions, developers who do the work and below that people who send patches to the developers.  It's only when there's a lack of direction that the next level up.  In FreeBSD there is a core team and sub-teams who send reports.  Having some level of formal communication like monthly reports goes a long way in helping with communication.  The basic structure is what Kulow has suggested and it works well for us. </p>

<p>Mirko: we have more than the technical group to talk about although that will be the largest one, but there are others.</p>

<p>Zack: we all agree on how the structure should look like.  Nobody wants to be directed.  We should concentrate on how the teams are created. How does FreeBSD work?</p>

<p>Lauri: core team is four people, there's release team which is a few more and they're self organised and vote for who should be in it.</p>

<p>Zack: so what's it for, conflict resolution isn't enough, that's how the idea started, we need more direction. Questions are what the goal of the team should be, how big the team, how to pick it.</p>

<p>Mirko: What is responsibility of the core team?</p>

<p>Zack: decide the right direction that KDE should have.  Conflict resolution doesn't have to be in KDE a lot of times.  </p>

<p>Lauri: conflict resolution is confrontational, it should be more creating direction and deciding goals.</p>

<p>Konold: we should have conflict resolution but it's not our main topic.  He wouldn't look officially at FreeBSD as they're different, they have a good plan of what to do over the next 10 years, we are different, they have standards like Posix, we have a larger direction and need structures which work faster.  It's less about conflict resolution and that's exciting.</p>

<p>Cornelius: maybe we should separate conflict resolution from other stuff</p>

<p>Zack: think that's too confusing, too much structure</p>

<p>Ian Geiser: if anyone has read core-devel for a long period of time, there are long threads which go on for a long time then stop.  This is where we need arbitration.  We need someone to say time out, this discussion is no longer productive, otherwise the thread just stops and someone does something in CVS and we moan about it because we can't then remove it.  </p>

<p>Mirko: he would like to define what groups to make.</p>

<p>Stephan Kulow gives an example of a game that was up for removal, but since he is not elected if he makes a decision it annoys people.  In the feature list someone added a feature which was committed but made things very unstable, the author said it was in the feature list so you should have objected there, would be good to have a group to make decisions on things like that.  If you have a feature that you aren't sure is feasible ask this group before someone opposes to this.</p>

<p>Eric Laffoon: looking back to his experience of directing a module, last year they started talking to KDevelop guys because they were duplicating a lot of efforts, there was a lot of work that they can now share.  There's stuff happening in KDE that he has no idea about.  There's nobody that takes information to exchange it between groups.  We want to produce more and should make groups that are responsible for knowing what's going on and can create better activity.  </p>

<p>Benjamin Meyer: He wants to know what groups to form.  Marketing, technical, suggests technical direction to create APIs etc.</p>

<p>Stephan: you don't want a working group so people have to join to work on API, you want one that says "here's something that's missing"</p>

<p>Ben: yes, just to watch</p>

<p>Jon Tapsel: examples of things he'd like to see.  Konversation, he'd like someone concrete able to say "yes, put this into KDE 4" to replace Ksirc.  Another thing is the text to speech and he'd like an official thing to say "yes we want all apps to support this nicely" because he doesn't know if all apps should support it.  He'd like a global address book contacts for the user's contact details but that needs lots of coordination on all the apps.  </p>

<p>Stephan: the second two examples come down to having a group direction</p>

<p>Jon Tapsel: yes, that's not conflict resolution it's asking for direction.</p>

<p>Aaron: what you're describing doesn't require the working group to do the work, but requires the working group to ensure the work gets done.  That's something in the past that we've been worried about burning people out but this is people just making sure it gets done, not done itself.</p>

<p>Konold: transparency is important, people should be able to make decisions.  In a company you have management people saying "i'm not the expert but I make the decisions, give me all the information".</p>

<p>Stephan: if you were in Zack's talk you'd know that transparency is now an official feature.</p>

<p>Mirko: this organisation has a board and we need to coordinate with the board.  We have board meeting which consist of phone conferences between Eva and himself.  They were asking what's the thing to do, they knew about conferences and things that are coming up, some requests for people wanting funding, but they had no general overview of what's going on in KDE.  They had a vision of how they'd like to work with this group of over 100 people.  They should in future like to have board meeting with people from all the different working groups, so they have the information of how to distribute the funding they have.  They want groups to function in such a way so the board knows how they're going to do their work.  </p>

<p>Konold: wants to make a proposal, we come up with an initial list of working groups, open for appending in the future, we check out who is interested in these different working groups.  Some of them can probably be self organised.  If they can't that's a problem we have to solve.  After the working groups get formed they can apply for formal approval by the board and maybe membership.  This approval gives the real power to these people so people can make decisions.  </p>

<p>Cornelius: how would these groups be related to e.V.  Would it be necessary that all members are members of e.V. or just one?  Are they not related at all?  Can they apply as a group to e.V.?  This makes a big difference to how groups can form.</p>

<p>Konold: it's a theoretical thing, if someone comes up with a group they are probably also eligible for being a member of e.V.</p>

<p>Mirko: would like to delay that, although it's an important decision.  Wants a brainstorm for groups.</p>

<h3>Brainstorm</h3>

<ul>
<li>marketing.</li>
<li>website, separately</li>
<li>documentation</li>
<li>translation</li>
<li>rather than documentation, technical education which includes documentation</li>
<li>security</li>
<li>release coordination</li>
<li>conference organisation</li>
<li>sysadmin</li>
<li>standard representation</li>
<li>accessibility</li>
<li>usability and graphics design/artists</li>
<li>multimedia</li>
<li>pim</li>
<li>portability</li>
<li>fundraising</li>
<li>alliance management with companies</li>
<li>quality assurance (much laughter)</li>
<li>freedesktop.org and standards groups</li>
<li>working group coordination (more laughter)</li>
<li>administrative tasks for e.v.</li>
<li>education</li>
<li>new developer integration, recruitment</li>
<li>long term strategy innovation</li>
<li>bug management</li>
<li>collaboration</li>
<li>legal</li>
</ul>

<p>Discussion on which groups, how many, do we need groups for every aspect.  For some things one coordinator is enough.</p>

<p>Discussion on whether groups are top level or a subtask of another group.</p>

<p>We go through the list and deciding which ones to keep and which ones to be part of others.</p>

<p>Konold gets out a mind map program.  Until the program crashes.</p>

<p>Gets down to 4 major topics</p>

<ul>
<li>administration (non-technical)</li>
<li>technical (software stuff)</li>
<li>human interfaces (making software usable)</li>
<li>marketing (PR)</li>
</ul>

<p>Discussion on what to look over this.  The board and the membership.  Suggestions for a core group.</p>

<p>Aaron tries mind mapping again, this time with kdissert.</p>

<p>Discussion on separation of technical and human/computer interfaces.</p>

<p>Discussion on what happens if there's a conflict between the two groups, e.V. membership vote is too slow.  That can be worked around.</p>

<p><em>Break</em></p>

<p>Mirko sums up.  We have 4 groups and a core group made up of 1 person from each group and the board.  On top of that is the membership.</p>

<p>Geiser: last 2 hours are a great example of how to make a decision.  </p>

<p>Konold: want to propose that we summarise and agree</p>

<p>Christian: not sure that we agreed on the central group</p>

<p>Konold: that's what he meant to agree.  Membership is top so that's respected.  For daily decisions we have the board, that is chosen by the membership.  Daily work is by the groups.</p>

<p>Faure: do we want the board involved in every conflict?</p>

<p>Mirko: most work done by workgroups and below, group made of 1 from each workgroup and board meets every few months, membership every year.</p>

<p>Aaron: what we're talking about is being able to communicate, coordinate and delegate.  Not about conflict resolution, that's a later discussion.</p>

<p>Konold: who has a problem with that?  </p>

<p>Cornelius: concern is that by adding this structure we create problems we didn't have before because we create something that doesn't grow dynamically because we've discussed here and made a plan.  Currently we have some structure which has grown from the project.  By creating these we might not get the right groups and conflicts can arise from the fact we rely on them.  </p>

<p>Mirko: this is coming out of the project.  Currently we have groups that don't have long term plans and don't have budget.  </p>

<p>Geiser: understands where Cornelius is coming from, where the object is that he feels we're pigeon-holing things whereas in the past with other organisations there's the concept of an ad-hoc group and when that becomes active enough and has demands there's an official group.  Active groups like the PIM group, the Netherlands etc those have a natural affinity for each other and may become more formal as we go on.  </p>

<p>Konold: understands from Cornelius that it looks like a top down process.  From Ian he understands he questioned the need.  But we had a vote which we should accept from the assembly.  </p>

<p>Faure: not questioning the vote questioning the way it has been cut down.</p>

<p>Geiser: there's no doubt of the need, groups have structured themselves and we need a formality in place.</p>

<p>Cornelius: we have to be careful not to create groups just by us wanting to have them, we want a marketing group but if there's no people then it's not sustainable.  We should base it on what groups we currently have.</p>

<p>Richard Moore: likes the structure but not sure how to get to it, should be a blessing mechanism for people who deal with X.  </p>

<p>Mirko: not trying to get these specific group, but is trying to get groups he and the board can work with.</p>

<p>Faure: we had brainstorming and cut it down into what we do in KDE.  There is a difference between categories of what we do and categories of what we need.  </p>

<p>Olaf Schmidt: problem with having ad-hoc groups officialised is that we end up with 30 groups coming to the board meeting.</p>

<p>Aaron: get Jonathan Riddell to put together a summary, pass it round to proofread and forward to e.V. membership</p>

<p>Mirko: does anyone want to coordinate efforts to decide a proposal for a technical group?</p>

<p>Geiser and Zack and Thiago puts up hands.  Kulow and Faure get in too.</p>

<p>Administration seems to be too broad.  The e.v. board is into administration.  Narrow it to sysadmin.  Or no group for it just now.</p>

<p>Dirk Mueller: but e.g. domain names and Subversion accounts need decision making.</p>

<p>Mirko: anyone want to get a proposal for a Human-Computer Interaction group?</p>

<p>Olaf, Gunnar Schmidt agree.  Probably Ken Wimer will want to be on it.</p>

<p>John Tapsel and Martijn and Kurt for marketing.</p>

<?php
include "footer.inc";
?>

