<?php
$page_title = "2002 Meeting Report";
include "header.inc";
?>

<h2>KDE e.V. meeting report -- meeting held 24th to 28th August 2002, Hamburg, Germany</h2>
<p>By <a href="mailto:howells@kde.org">Chris Howells</a> and
<a href="mailto:danimo@kde.org">Daniel Molkentin</a>, KDE Policies SIG notes from
<a href="mailto:faure@kde.org">David Faure</a>.</p>
<h2>Thank you...</h2>

<p>I would like to start this report by saying various thanks. I would like to thank
<a href="http://www.unibw-hamburg.de">Universitaet der Bundeswehr</a> (University 
of the Federal Armed Forces) in Hamburg by generously providing access to its facilities 
including accomodation, meeting rooms, a computer lab. I would like 
to thank Mirko Boehm, the treasurer of KDE e.V for organizing the meeting.</p>

<p>Additionally I would like to congratulate the new board on their election: Ralf Nolden,
Eva Brucherseifer, Kalle Dalheimer, and Mirko Boehm, and I wish them the best of 
luck in helping the KDE Project through KDE e.V.</p>

<h2>About KDE e.V.</h2>

<p><a href="http://www.kde.org/kde-ev">KDE e.V.</a> is a registered non-profit organization 
set up to represent the KDE Project in legal and financial matters, by supporting 
the developers. Furthermore, it collaborates with <a href="http://www.trolltech.com">Trolltech</a>
through the <a href="http://www.kde.org/kdeqtfoundation.html">FreeQt Foundation</a> and sends
 Officers to the KDE League.</p>

<p>All persons actively and usefuly contributing to KDE e.V are welcome to apply 
to become members of the organization.</p>

<h2>Aims</h2>

<p>The meeting of KDE e.V. members had several aims. The most essential of 
these was to elect a new board, since only one member of the old board was still 
actively working on KDE. Other aims were to approve the financial report for 
KDE e.V., and to discuss the future of KDE in some Special Interest Groups (SIGs).</p>

<p>Additonally, the meeting provided an excellent opportunity to meet and socialise 
with fellow KDE developers, often for the first time in person.</p>

<p>The following candidates were elected to the board of KDE e.V.</p>

<ul>
<li><a href="mailto:kalle@kde.org">Matthias "Kalle" Dalheimer</a> -- President</li>
<li><a href="mailto:mirko@kde.org">Mirko Boehm</a> -- Treasurer</li>
<li><a href="mailto:eva@kde.org">Eva Brucherseifer</a> -- Board Member</li>
<li><a href="mailto:nolden@kde.org">Ralf Nolden</a> -- Board Member</li>
</ul>

<p>Additonally <a href="mailto:lars@kde.org">Lars Knoll</a> and <a href="mailto:
dirk@kde.org">Dirk Mueller</a> were approved as auditors of the KDE e.V. accounts.</p>

<p>The administrative matters were finished as soon as possible by Sunday 25th August 
in order to leave time to progress to other areas of the schedule such as hacking on KDE in the
computer lab and working in the SIGs.</p>

<h2>KDE hacking progress</h2>

<p>Some hacking on KDE went on in the computer lab. Of particular interest,
<a href="mailto:coolo@kde.org">Stephan Kulow</a> continued to work on the new <a 
href="http://bugs.kde.org">Bugzilla bugs database</a> which recently went live. 
<a href="mailto:cullmann@kde.org">Christoph Cullmann</a> and <a href="mailto:faure@kde.
org">David Faure</a> worked on integrating the new kfind library into <a href="http:
//kate.kde.org">Kate</a> and khtml, respectively. Cornelius Schumacher did first 
steps towards turning KMail into a KPart and Matthias Welwarsky improved KPlayObject's 
streaming capabilities.</p> 

<p>Various bugs were also fixed.</p>

<h2>Key signing</h2>

<p>The meeting featured a keysigning session. It was meant as both a preparation 
for online voting in KDE e.V. as well as increasing security for releases, since all new KDE 
releases are from now on signed with the GnuPG key of the respective release dude 
or contributor. The keysigning was conducted by <a href="mailto:molkentin@kde.org"
>Daniel Molkentin</a> and <a href="mailto:mutz@kde.org">Marc Mutz</a>. Marc also 
provided a crash course on GPG and gave last instructions for the signing procedure.
</p>

<h2>SIGs</h2>

<p>The SIGs were intended to allow interested persons to discuss specific areas of KDE
in a highly targetted and specific manner.</p>

<p>Some of the topics discussed in the groups included:</p>

<ol>
<li>Trademark</li>
<li>KDE Policies/Culture</li>
<li>Formation of a company based on selling KDE services</li>
<li>KDE Edutainment project</li>
<li>KDE PIM and Groupware</li>
<li><i>and others...</i></li>
</ol>

<h2>Results of the SIGs</h2>

<h3>Trademark</h3>
<p>One of the decisions made during the meeting was to register the "KDE" trademark 
with KDE e.V. It was decided that Kalle would register the trademark in Sweden (since 
this will automatically apply to the whole of the EU). A US member of KDE e.V. will 
apply for the trademark in the United States.</p>

<h3>KDE Policies</h3>
<p>KDE e.V. is the organizational body behind many assets such as KDE's mail and CVS servers. Several years
ago it was acceptable to have less rigorous guide lines regarding access to various resources.
However in line with the huge growth of the project there was a suggestion that access to some resources
may need to be controlled more closely.</p>

<p>The following is a summary of the discussions (which is neither complete nor authoritive). Nothing is set in
stone yet and some matters will be discussed further on the
<a href="http://mail.kde.org/mailman/listinfo/kde-policies">kde-policies</a> mailing list.</p>

<h4>E-Mail addresses</h4>

<p>The kdemail.org domain will be obtained in order to give a "stable" e-mail address
to occassional KDE contributors, such as developers developing out of CVS and third party application developers.
This would be in preference to being given a kde.org e-mail address immediately. This will help to reduce the
impression that the media and public have that some people "speak for the KDE Project", simply because they
have a kde.org e-mail address.</p>

<p>In order to get a kde.org e-mail address the applicant must have shown a commitment to KDE for
some time (this will be judged by the sysadmins), and the applicant must agree with the KDE
Culture guidelines (see below).</p>

<h4>CVS accounts</h4>

<p>Suggested policy for obtaining a CVS account will remain the same as current (you ask for it,
explaining why you need it -- commits can always be reverted).</p>

<p>The use of SSH authorisation will be investigated for use in conjunction with more privileged
accounts (e.g. those able to use cvs tag, since these operations can't easily be reverted).</p>

<h4>Policy for becoming a member of KDE e.V.</h4>
<p>The applicant must fulfill the requirements for obtaining a kde.org e-mail address
(although of course the applicant does not need to have a kde.org e-mail address already).</p>

<p>The KDE e.V. bylaws will be changed to say that the applicant must be invited to become a member,
and that the applicant must be supported by two active members of KDE e.V.</p>

<h4>Policy for uploading to ftp.kde.org</h4>

<p>There is a need to know who uploads what. For example, an e-mail address must be associated
with every uploaded file to be able to trace the uploader of a trojan.</p>

<p>One possible solution is that the uploader goes to a web page, enters his/her e-mail
address, obtains a one-time login and password via e-mail, and then uses this for the upload.</p>

<h4>Policy for choosing release co-ordinator</h4>

<p>There is a possibility of electing a co-ordinator via e-mail (with GnuPG signed messages), where
everybody with a kde.org e-mail address could vote. The current system sometimes leads to controversy
because there is no clearly defined procedure for choosing a new co-ordinator when the previous
one re-signs.</p>

<p>Note: this would not change the current policy that voting is <b>not</b> used to control
developement.</p>

<h4>Example policies</h4>

<p>These guidelines arose from the discusssions in the KDE Policies SIG, but were
later refined in the KDE Culture SIG, and are to be refined further on the
kde-policies mailing list.</p>

<p>It is suggested that contributors should adhere to the following guidelines:</p>

<ul>
<li>No bashing other projects (e.g. free software/open source).</li>
<li>Showing tolerance to others at all times.</li>
<li>Avoiding the discussion of potentially offensive/controversial material (e.g. pornography,
violence, politics, religion).</li>
<li>Adhereing to the free software idea.</li>
<li>Remembering that no single contributor represents the KDE Project or KDE e.V.</li>
</ul>

<p>Not following these guidelines could lead to reprimands such as removal of kde.org 
e-mail addresses.</p>

<p>Once the new guidelines are finalized, we will send a mail to everyone
with a kde.org email address, to announce the guidelines and ask people to
respect it (or drop their email address). As a nice side-effect, this will
allow cleaning up of all the kde.org addresses that bounce.</p>

<h3>KDE PIM</h3>
<p>The group, consisting of KMail and KDE PIM hackers, worked out problems with 
the current PIM components and their possible resolution. It was also concluded 
that there is a huge demand for an integrated groupware solution for Linux especially 
for corporate use. A small roadmap was made including the plan to turn <a href="http:
//kmail.kde.org">KMail</a> into a KPart and work on a Kaplan-based solution.</p>

<h2>Some clarification on structure</h2>

<h3>Responsiblity of the KDE Bodies</h3>
<p>By popular demand, the relations and tasks of the various KDE bodies were explained and 
discussed.
</p>

<h3>KDE e.V.</h3>

<p><a href="http://www.kde.org/kde-ev/">KDE e.V.</a> is a representation 
of the KDE Developers and is therefore responsible for staffing and organizing fairs 
while funding for such events is organized done by the KDE League. However KDE e.V. can step 
in to pay minor expenses, too.</p>

<p>KDE e.V.'s scope does not extend to technical discussions. KDE e.V. holds the trademark 
and the copyright for KDE the core brands.</p>


<h3>KDE League</h3>

<p>The KDE League
also is responsible for <a href="http://www.kde.org/announcements/">press releases and announcements</a>,
printing flyers and brochures and doing presentations to developers, businesess and governments. Both KDE e.V. and the
KDE League do their own fundraising for their respective tasks while the KDE Leage also gets funds from their members.</p>

<h3>Relations between the bodies</h3>
<p>The KDE e.V. member assembly elects 4 board members as well as officers to cooperate 
with other bodies:
</p>
<ul>
<li>3-6 officers are sent to the KDE League Board. Two of them are sent into the League's 
executive board</li>
<li>2 officers are sent to represent KDE in the <a href="http://www.kde.org/kdeqtfoundation.
html">KDE Free Qt Foundation</a>.</li>
</ul>
<p>
KDE e.V. has a majority in the League so that its members will always have full 
control over the League. This however did have the unfortuante effect that due to the inactivity in 
KDE e.V., the League was almost unable to act. However, it has been decided that new officers 
will be elected for those positions so the League can finally continue more active.
</p>

<h2>Photos</h2>
<ul>
<li><a href="http://developer.kde.org/~howells/kdeev-photos/1600x1200/group.jpg">Group Photo</a></li>
<li><a href="http://developer.kde.org/~howells/kdeev-photos">General Gallery 1</a></li>
<li><a href="http://developer.kde.org/~matz/photos">General Gallery 2</a></li>
</ul>

<?php
include "footer.inc";
?>
