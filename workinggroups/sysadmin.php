<?php
 $page_title = "KDE e.V. System Administration Working Group";
 include "header.inc";
?>

<h2>Mission</h2>

<p>The System Administration Working Group is responsible for the administration
of the KDE server infrastructure.</p>

<h2>Members</h2>

<ul>
<li>Daily active sysadmins: Nicolás Alvarez, Ben Cooksley
<li>DevOps: Boudhayan Gupta, Scarlett Gately Clark
<li>Sysadmin/webadmin: Ingo Malchow
<li>Less active sysadmins: Eike Hein, Jeff Mitchell
<li>Sporadically active sysadmins: David Faure, Dirk Mueller
</ul>

<h2>Contact</h2>

<p>You can reach the System Administration Working Group by mail at
<a href="mailto:sysadmin@kde.org">sysadmin@kde.org</a>.</p>

<?php
 include "footer.inc";
?>
