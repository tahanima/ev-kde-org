---
title: "KDE e.V. Trusted IT Consulting Firms"
layout: page
---

<style>
.img-consultant {
  float: left;
  border: 1px solid #aaa;
  margin: 15px;
  margin-top: 7px;
  margin-left: 0;
  padding: 5px;
  border-radius: 3px;
}
</style>

## KDE e.V. and Consulting Firms

In KDE we take pride in being a free software community and having an open,
free and fair development process.

However, we also understand that sometimes companies and institutions main
priority is not learning the processes of our community and they just want
issue solved or a feature implemented.

For this reason, we are offering here a list of consultants that have
expertise in dealing with the KDE Community and we know will help get your
project landed in KDE as best as possible.

We encourage you to contact them directly, but if you feel we can help you,
do not hesitate to contact us at
<a href="mailto:consulting.org">consulting@kde.org</a>.

## KDE Patrons

{% for consultant in site.data.consultants.patrons %}
  <h3>{{ consultant.name }}</h3>
  <div>
    <img class="img-consultant" width="100" src="{{ consultant.logo }}" />
    {{ consultant.description }}
  <div>
  <p class="text-right"><a href="{{ consultant.url }}">{{ consultant.url }}</a></p>
{% endfor %}


<h2 style="clear:both;">KDE Supporters</h2>

{% for consultant in site.data.consultants.supporters %}
  <h3>{{ consultant.name }}</h3>
  <div>
    <img class="img-consultant" width="100" src="{{ consultant.logo }}" />
    {{ consultant.description }}
  <div>
  <p class="text-right"><a href="{{ consultant.url }}">{{ consultant.url }}</a></p>
{% endfor %}

<h2 style="clear:both;">Other Consulting Firms</h2>

{% for consultant in site.data.consultants.other %}
  <h3>{{ consultant.name }}</h3>
  <div>
    <img class="img-consultant" width="100" src="{{ consultant.logo }}" />
    {{ consultant.description }}
  <div>
  <p class="text-right"><a href="{{ consultant.url }}">{{ consultant.url }}</a></p>
{% endfor %}

<h2 style="clear:both">Your company on this page</h2>

<p>We are more than happy of adding trusted consultants to this page. Please see
<a href="consultants_join.html">this page</a> for more information.
