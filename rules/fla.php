<?php
 $page_title = "Fiduciary Licensing Agreement";
 include "header.inc";
?>

<h2>Fiduciary Licensing Agreement</h2>

<p>
At the General Assembly of KDE e.V. in August 2008
the membership voted to adopt a Fiduciary Licensing Agreement
as the preferred form for assigning copyright to KDE e.V.
</p>

<p>
Copyright assignment is a personal act. It is entirely <i>optional</i>
and at an individual developer, contributor or copyright holder's
discretion whether to assign copyright to KDE e.V. or not.
While there are various legal avenues available to
do such an assignment, a Fiduciary License Agreement
is one that preserves the spirit of Free Software
and is easy to administer.
</p>

<p>
The form for the Fiduciary Licensing Agreement
is available from the <a href="http://ev.kde.org/resources/">resources</a>
page.
</p>

<p>The Relicensing Policy restricts the ways in which KDE e.V.
can relicense the code for which it holds the copyright.
</p>

<p>
The Fiduciary Licensing Agreement
was created in cooperation with the
<a href="http://www.fsfeurope.org/projects/ftf/fla.en.html">Free
Software Foundation Europe</a>.
</p>

<h3>Versions of the FLA</h3>

<p>The text of the FLA and the relicensing document
has changed over time. Only the text of the particular
document you may have signed applies. Most changes are 
of minor importance, but they are documented here.
</p>

<ul>
<li><b>1.3.5</b> Unified the prefab and non-prefab document.</li>
<li><b>1.3.4</b> Updated post address of KDE e.V.</li>
<li><b>1.3.3</b> Updated post address of KDE e.V.</li>
<li><b>1.3.2</b> (with associated FRP version 1.3.1) Added FLA with boilerplate text for KDE SVN account holders.</li>
<li><b>1.3.1</b> Clarify text and remove some licenses from FRP that we don't actually want to use.</li>
<li><b>1.3</b> Clarify text by referring to specific version of KDE e.V. charter.</li>
<li><b>1.2</b> Changes of address.</li>
<li><b>1.2</b> Changes in the way the document is created.</li>
<li><b>1.1</b> Initial version (the FSFE document is considered version 1.0).</li>
</ul>

<h3>Signers of the FLA</h3>

<p>A lot of KDE contributors have signed the FLA. Among them are the following people (the numbers following the names indicate the version of the FLA that they have signed):
</p>

<ul>
<li>Adriaan de Groot (1.3.1)</li>
<li>Adrián Chaves Fernández (1.3.3)</li>
<li>Aitor Pazos Ibarzabal (1.3.3)</li>
<li>Àlex Fiestas (1.3.3)</li>
<li>Aleix Pol Gonzalez (1.3.3)</li>
<li>André Wöbbeking (1.3.3)</li>
<li>Andreas Cord-Landwehr (1.3.3)</li>
<li>Andreas Hartmetz (1.3.2)</li>
<li>Bhushan Shah (1.3.4)</li>
<li>Casper van Donderen (1.3.2)</li>
<li>Chani Armitage (1.3.2)</li>
<li>Christoph Cullmann (1.3.5)</li>
<li>Claus Christensen (1.3.4)</li>
<li>Cornelius Schumacher (1.3.1)</li>
<li>Cristian Tibirna (1.3.3)</li>
<li>Dan Leinir Turthra Jensen (1.3.3)</li>
<li>Daniel Laidig (1.3.3)</li>
<li>Daniel Vrátil (1.3.5)</li>
<li>David Edmundson (1.3.4)</li>
<li>Dominik Haumann (1.3.2)</li>
<li>Dominique Devriese (1.3.4)</li>
<li>Eckhart Wörner (1.3.2)</li>
<li>Elvis Angelaccio (1.3.5)</li>
<li>Franck Arrecot (1.3.4)</li>
<li>Frederik Gladhorn (1.3.4)</li>
<li>Harald Fernengel (1.3.3)</li>
<li>Ingo Klöcker (1.3.2)</li>
<li>Ivan Cukić (1.3.3)</li>
<li>John Layt (1.3.3)</li>
<li>Kevin Funk (1.3.4)</li>
<li>Kevin Krammer (1.3.3)</li>
<li>Kévin Ottens (1.3.4)</li>
<li>Luigi Toscano (1.3.3)</li>
<li>Lukas Sommer (1.3.3)</li>
<li>Lydia Pintscher (1.3.2)</li>
<li>Marco Martin (1.3.3)</li>
<li>Mark Gaiser (1.3.4)</li>
<li>Martin Gräßlin (1.3.3)</li>
<li>Matěj Laitl (1.3.4)</li>
<li>Matthias Ettrich (1.3.2)</li>
<li>Michael Bohlender (1.3.4)</li>
<li>Milian Wolff (1.3.2)</li>
<li>Miquel Canes Gonzalez (1.3.3)</li>
<li>Oswald Buddenhagen (1.3.2)</li>
<li>Pradeepto Bhattacharya (1.3.5)</li>
<li>Rafael Fernández López (1.3.2)</li>
<li>Rajeesh K V (1.3.4)</li>
<li>Reinhold Kainhofer (1.3.1)</li>
<li>Riccardo Iaconelli (1.3.3)</li>
<li>Rishab Arora (1.3.4)</li>
<li>Rohan Garg (1.3.3)</li>
<li>Sandro Knauß (1.3.4)</li>
<li>Scarlett Clark (1.3.4)</li>
<li>Sebastian Kügler (1.3.1)</li>
<li>Sébastien Renard (1.3.3)</li>
<li>Shantanu Tushar (1.3.4)</li>
<li>Simon Wächter (1.3.4)</li>
<li>Stefan Derkits (1.3.4)</li>
<li>Sune Vuorela (1.3.5)</li>
<li>Teo Mrnjavac (1.3.4)</li>
<li>Till Adam (1.3.2)</li>
<li>Torrie Fischer (1.3.3)</li>
<li>Valorie Zimmermann (1.3.4)</li>
<li>Vishesh Handa (1.3.3)</li>
<li>Volker Krause (1.3.5)</li>
</ul>

<?php
include "footer.inc";
?>
