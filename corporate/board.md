---
title: People
layout: page
board:
  - name: Aleix Pol i Gonzàlez
    title: President
    email: aleixpol<span>@</span>kde.org
    description: Aleix Pol i Gonz&agrave;lez has been collaborating with KDE since 2007. He started working in
      software development in the KDE Education area and KDevelop.
      Aleix joined the KDE e.V. board of directors in 2014.
      In his day-job, he is employed by BlueSystems where he has worked with other parts of the community
      including Plasma and Qt.
    image: pictures/apol.jpg
  - name: Eike Hein
    title: Treasurer and Vice President
    email: hein<span>@</span>kde.org
    description: Eike Hein has been a KDE contributor since 2005. Initially working
      on applications and later on Plasma as developer and designer, he has also served
      on KDE's Sysadmin team and co-authored the <a href="https://manifesto.kde.org/">KDE Manifesto</a>.
      Eike joined the KDE e.V. board of directors in 2017.
    image: pictures/eike.jpg
  - name: Lydia Pintscher
    title: Vice President
    email: lydia<span>@</span>kde.org
    description: Lydia Pintscher has been with KDE since 2006. She started doing
      marketing, and later community and release management for Amarok. She
      has since moved on to community management and running mentoring
      programs for all of KDE. She is on the board of directors of KDE e.V.
      since 2011. In her day-job she does product management for
      Wikidata at Wikimedia Germany.
    image: pictures/lydia.jpg
  - name: Neofytos Kolokotronis
    title: Board Member
    email: neofytosk<span>@</span>kde.org
    description: Neofytos has been a contributor to FOSS and Open Data/Government
      projects for nearly a decade, serving from a variety of positions. He joined KDE
      a couple of years ago via the Promo team, however when the KDE goal he proposed on
      "Streamlined Onboarding of new contributors" was voted by the community, he had to
      turn his full attention to leading it and pushing for achieving its objectives. He
      is also an elected member of the e.V.'s Financial Working Group..
    image: pictures/neofytos.jpg
  - name: Adriaan de Groot
    title: Board Member
    email: groot<span>@</span>kde.org
    description: Adriaan encountered KDE in 1999 and has been involved in various roles
      ever since. He served on the board of KDE e.V. 2006-2011, and participated in KDE PIM
      development, FreeBSD porting, Solaris porting, the creation of the English Breakfast
      Network (for code-quality checking) and api.kde.org. He can usually be found in the
      Netherlands and works for Blue Systems as the maintainer of Calamares, the independent
      Linux System Installer as well as doing contract work for Free Software projects.
    image: pictures/adriaan.jpg
---

# People

KDE e.V. elects a board of directors, which represents KDE e.V.
and runs its business.

You can contact the board of KDE e.V. at <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.

## Board of Directors

{% for people in page.board %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="{{ people.image }}" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.name }}</h3>
    <p class="people-title">{{ people.title }}</p>
    <p class="people-email">{{ people.email }}</p>
    <p>{{ people.description }}</p>
  </div>
</div>
{% endfor %}

## Previous Boards

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load("current", {packages:["timeline"]});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var container = document.getElementById('boardtimeline');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();

    dataTable.addColumn({ type: 'string', id: 'Term' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });

    var president = 'President'
        , treasurer = 'Vice President & Treasurer'
        , vp = 'Vice President'
        , boardA = 'Board Member A'
        , boardB = 'Board Member B'
    ;
    dataTable.addRows([
      [ president,  'Matthias Ettrich', new Date(1997, 11, 26), new Date(1999, 10, 09) ],
      [ president,  'Kurt Granroth', new Date(1999, 10, 09), new Date(2002, 08, 25) ],
      [ president,  'Kalle Dalheimer', new Date(2002, 08, 25), new Date(2005, 08, 26) ],
      [ president,  'Eva Brucherseifer', new Date(2005, 08, 26), new Date(2007, 11, 04) ],
      [ president,  'Aaron Seigo', new Date(2007, 11, 04), new Date(2009, 07, 07) ],
      [ president,  'Cornelius Schumacher', new Date(2009, 7, 7), new Date(2014, 08, 22) ],
      [ president,  'Lydia Pintscher', new Date(2014, 08, 22), new Date(2019, 09, 09) ],
      [ president,  'Aleix Pol i Gonzàlez', new Date(2019, 09, 09), new Date(2019, 12, 31) ],
      [ treasurer,  'Kalle Dalheimer', new Date(1997, 11, 25), new Date(1999, 10, 9) ],
      [ treasurer,  'Mirko Böhm', new Date(1999, 10, 9), new Date(2005, 08, 26) ],
      [ treasurer,  'Cornelius Schumacher', new Date(2005, 08, 26), new Date(2009, 7, 7) ],
      [ treasurer,  'Frank Karlitschek', new Date(2009, 7, 7), new Date(2012, 07, 03) ],
      [ treasurer,  'Agustín Benito Bethencourt', new Date(2012, 07, 03), new Date(2014, 8, 22) ],
      [ treasurer,  'Marta Rybczynska', new Date(2014, 8, 22), new Date(2017, 7, 22) ],
      [ treasurer,  'Eike Hein', new Date(2017, 7, 22), new Date(2019, 12, 31) ],
      [ vp,         'Martin Konold', new Date(1997, 11, 25), new Date(1999, 10, 9) ],
      [ vp,         'Chris Schläger', new Date(1999, 10, 9), new Date(2002, 8, 25) ],
      [ vp,         'Eva Brucherseifer', new Date(2002, 8, 25), new Date(2005, 08, 26) ],
      [ vp,         'Mirko Böhm', new Date(2005, 08, 26), new Date(2006, 09, 25) ],
      [ vp,         'Adriaan de Groot', new Date(2006, 09, 25), new Date(2011, 08, 09) ],
      [ vp,         'Sebastian Kügler', new Date(2011, 08, 09), new Date(2013, 7, 12) ],
      [ vp,         'Lydia Pintscher', new Date(2013, 07, 12), new Date(2014, 8, 22) ],
      [ vp,         'Aleix Pol i Gonzàlez', new Date(2014, 8, 22), new Date(2019, 09, 09) ],
      [ vp,         'Lydia Pintscher', new Date(2019, 09, 09), new Date(2019, 12, 31) ],
      [ boardA,     'Michael Renner', new Date(1997, 11, 25), new Date(1999, 10, 9) ],
      [ boardA,     'Preston Brown', new Date(1999, 10, 9), new Date(2002, 8, 25) ],
      [ boardA,     'Marie Loise Nolden', new Date(2002, 8, 25), new Date(2004, 08, 20) ],
      [ boardA,     'Harry Porten', new Date(2004, 08, 20), new Date(2005, 08, 26) ],
      [ boardA,     'Aaron Seigo', new Date(2005, 08, 26), new Date(2007, 11, 04) ],
      [ boardA,     'Sebastian Kügler', new Date(2007, 11, 04), new Date(2011, 08, 09) ],
      [ boardA,     'Lydia Pintscher', new Date(2011, 08, 09), new Date(2013, 7, 12) ],
      [ boardA,     'Albert Astals Cid', new Date(2013, 7, 12), new Date(2016, 9, 1) ],
      [ boardA,     'Thomas Pfeiffer', new Date(2016, 9, 1), new Date(2019, 09, 09) ],
      [ boardA,     'Neofytos Kolokotronis', new Date(2019, 09, 09), new Date(2019, 12, 31) ],
      [ boardB,     'Klaas Freitag', new Date(2007, 11, 04), new Date(2009, 7, 7) ],
      [ boardB,     'Celeste Lyn Paul', new Date(2009, 7, 7), new Date(2012, 07, 03) ],
      [ boardB,     'Pradeepto Bhattacharya', new Date(2012, 07, 03), new Date(2015, 7, 24) ],
      [ boardB,     'Sandro Andrade', new Date(2015, 7, 24), new Date(2018, 08, 13) ],
      [ boardB,     'Andy Betts', new Date(2018, 8, 13), new Date(2019, 09, 09) ],
      [ boardB,     'Adriaan de Groot', new Date(2019, 09, 09), new Date(2019, 12, 31) ],
    ]);

    var options = {
        width: 1000
    };
    chart.draw(dataTable, options);
  }
</script>

<div id="boardtimeline" style="height: 400px; width: 90%"></div>

<?php
include "footer.inc";
?>
