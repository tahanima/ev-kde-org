<?php
 $page_title = "Akademy";
 include "header.inc";
?>

<p>
Since 2003 the KDE e.V. has organized an annual meeting of the KDE community. It
includes a conference, time for coding, meetings and
the annual membership meeting of the e.V.  It has been known under the name <a
href="http://akademy.kde.org">Akademy</a> since 2004. It is the biggest meeting
of the worldwide KDE community.
</p>

<!--<h2>Akademy 2014</h2>

<p>If you are interested in hosting an Akademy in the future, please have a look
at the <a href="CallforHosts_2014.pdf">2014 Call for Hosts</a>[pdf].</p>-->

<h2>Akademy History</h2>

<p>Up until 2008 the spelling "aKademy" was used. The name was changed to "Akademy" after that.</p>

<p>
<ul>
  <li><a href="https://akademy.kde.org/2019">Akademy 2019, Milan, Italy</a></li>
  <li><a href="https://akademy.kde.org/2018">Akademy 2018, Vienna, Austria</a></li>
  <li><a href="https://akademy.kde.org/2017">Akademy 2017, Almería, Spain</a></li>
  <li><a href="https://akademy.kde.org/2016">Akademy 2016 (as part of QtCon), Berlin, Germany</a></li>
  <li><a href="https://akademy.kde.org/2015">Akademy 2015, A Coruña, Galicia, Spain</a></li>
  <li><a href="https://akademy.kde.org/2014">Akademy 2014, Brno, Czech Republic</a></li>
  <li><a href="https://akademy.kde.org/2013/">Akademy 2013, Bilbao, Basque Country, Spain</a></li>
  <li><a href="https://akademy2012.kde.org">Akademy 2012, Tallinn, Estonia</a></li>
  <li><a href="https://www.desktopsummit.org">Akademy 2011 (Desktop Summit), Berlin, Germany</a></li>
  <li><a href="https://akademy2010.kde.org">Akademy 2010, Tampere, Finland</a></li>
  <li><a href="https://akademy2009.kde.org">Akademy 2009 (Desktop Summit), Las Palmas, Gran Canaria</a></li>
  <li><a href="https://akademy2008.kde.org">aKademy 2008, Sint-Katelijne-Waver, Belgium</a></li>
  <li><a href="https://akademy2007.kde.org">aKademy 2007, Glasgow, Scotland</a></li>
  <li><a href="https://akademy2006.kde.org">aKademy 2006, Dublin, Ireland</a></li>
  <li><a href="https://conference2005.kde.org">aKademy 2005, Malaga, Spain</a></li>
  <li><a href="https://conference2004.kde.org">aKademy 2004, Ludwigsburg, Germany</a></li>
</ul>
</p>

<?php
include "footer.inc";
?>
