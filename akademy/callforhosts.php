<?php
$page_title = "Call for Hosts for Akademy 2012";
include "header.inc";
?>


<p>
The KDE e.V. invites proposals to host Akademy, the yearly KDE contributors
conference and community summit, during the Summer of 2012. Akademy is the
biggest gathering of the community and includes a two-day conference, the
general assembly of the members of the KDE e.V., and a week of coding,
meeting, and discussing.
</p>

<p>Proposals for Akademy are warmly welcomed, and should
aim for 350-400 attendees.
</p>

<p>
Proposals for Akademy should be sent to <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.
Deadline for proposals is October 1st 2011, so that
the board has enough time to decide on proposals.
</p>

<p>
Key points which proposals should consider, and which will be taken into
account when deciding among candidates, are:
</p>

<ul>
<li>Local community support for hosting the conference</li>
<li>The availability and cost of travel from major European cities</li>
<li>The availability of sufficient low-cost accommodation</li>
<li>The budget for infrastructure and facilities required to hold the
conference</li>
<li>The availability of restaurants or the organization of catering on-site</li>
<li>Local industry and government support</li>
</ul>

<p>
See the <a href="CallforHosts_2012.pdf">detailed requirements pdf</a> for more information about what it needs to host Akademy.
</p>

<p>
The conference will require availability of facilities for one week,
including a weekend, during Summer. Dates should avoid other key free software
conferences.
</p>

<p>
A few words of advice: organizing a conference of this size is hard work, but
there are many people in the community with experience who will be there to
help you. Bear in mind that people coming to these conferences do so
primarily to meet up with old friends and have fun, and so the hallway track
and social activities are very important.
</p>

<?php
include "footer.inc";
?>
