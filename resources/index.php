<?php
 $page_title = "Forms";
 include "header.inc";
?>

<ul>

  <li><a href="supporting_member_application.pdf">Application for Supporting
  Membership</a>. Fill out this form
  when applying for a corporate supporting membership.</li>

  <li><a href="ev-questionnaire.text">New Member Questionnaire</a>. Ordinary
  members have to answer this questionaire when applying for membership in the
  KDE e.V. Please note that your membership has to be proposed by an existing
  member to the membership before.</li>

  <li>Expense Report Form. Please fill out this 
  form when handing in receipts for costs the board has accepted to reimburse 
  you for. These forms do not apply for trips to sprints, conferences, etc. those
  are governed by the <a href='../rules/reimbursement_policy.php'>Travel Cost Reimbursement Policy</a>.
  <ul>
    <li>For reimbursement <a href="expense-paypal.pdf">via PayPal</a>, use the <a href="expense-paypal.pdf">PayPal form</a>.</li>
    <li>For reimbursement <a href="expense-iban.pdf">via international bank transfer</a>, use the <a href="expense-iban.pdf">international bank transfer (IBAN) form</a>.</li>
    <li>For reimbursement via other means or if you do not understand which of the above forms to use, use the <a href="expense_report.pdf">traditional expense report form</a>.</li>
  </ul>
  </li>

  <li><a href="proxy_instructions.pdf">Proxy Instructions</a>. If you are an
  active member of the KDE e.V. and are unable to attend the general assembly
  you can give a proxy the right to vote for you. Fill out this form and make
  sure the chairman of the general assembly has received it before the beginning
  of the assembly.</li>

  <li><a href="vereinfachter_zuwendungsnachweis_update2019.pdf">"Vereinfachter
  Zuwendungsnachweis".</a> If you are donating money to the KDE e.V., you can
  use this form to make use of the German tax-exemption rules. For more 
  information, please refer to <a href="../donations-taxes-de.php">this page</a>
  (in German).</li>

  <li>Fiduciary Licensing Documents. The FLA can be used to assign the
  copyright in your contributions to KDE to KDE e.V.:
    <ul>
      <li><a href="FLA.pdf" title="FLA">FLA</a> (Fiduciary License Agreement)</li>
      <li>The <a href="FRP.pdf" title="FRP text">FRP</a> is used as a document to direct how (potential) relicensing would occur.</li>
    </ul>
  <a href="../rules/fla.php">General information</a> about the FLA is <a href="../rules/fla.php">available here</a>.
  </li>

  <li><a href="local_kde_org_agreement.pdf">Agreement about local KDE
  organization</a>. This agreement is used between KDE e.V. and a local
  organization representing KDE in a specific region. It grants the local
  organization the limited right to represent KDE locally and use the KDE
  trademark for that purpose. It's required that the local organization follows
  KDE e.V.'s goals and non-profit rules.</li>
</ul>

<?php
include "footer.inc";
?>
