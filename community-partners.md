---
title:  "KDE e.V. Community Partners"
layout: page
---

The <a href="/activities/partnershipprogram.html">KDE Community Partnership Program</a> provides a framework for cooperation with other communities, organizations and individuals to further our common goals. The Program is meant to facilitate joint activities, exchange of experience, and provide a formal base for situations where we can collaborate and help one another.

+ KDE e.V. is a <a href="http://www.qt.io/partners/">community partner of the Qt Project</a>.
+ KDE e.V. is a <a href="https://www.lyx.org/">community partner of Lyx</a>.
+ KDE e.V. is a <a href="http://randa-meetings.ch/">community partner of Verein Randa Meetings</a>.

<!-- We should add more here: Membership in OASIS, ODF Association or how it is called, etc. -->
