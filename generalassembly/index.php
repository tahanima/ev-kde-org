<?php
 $page_title = "General Assembly";
 include "header.inc";
?>

<p>The next general assembly of KDE e.V. takes place at the University of Milano-Bicocca in Milan, Italy, on 9th of September starting at 12:30.</p>

<p>All members of KDE e.V. are invited to attend the general assembly.</p>

<p>See the <a href="agenda.php">agenda</a> for what is planned to happen at the general assembly.</p>

<p>If you are not able to participate in the general assembly in person you can,
according to section 6.6 of the articles of association of the KDE e.V., ask
another member to act as proxy for you and execute your voting right. Fill out
the <a href="../resources/proxy_instructions.pdf">proxy form</a>, sign it and make sure that it's available to the chairperson
of the general assembly on the day of the assembly if you make use of this
option. Each active member can act as proxy for up to two other active
members.</p>

<p>Please print out the proxy form, fill it in then scan it. This should then be emailed to your proxy and the <a href="mailto:kde-ev-board@kde.org">board</a>.</p>

<p>If you aren't executing your voting rights personally or through a proxy for
two consecutive general assemblies your membership status will be changed from
active to passive, according to section 4.2 of the articles of association.
Passive and supporting members have the right to attend the general assembly,
but they have no voting rights.</p>

<p>The general assembly will be held at <a href="http://akademy.kde.org">Akademy</a>. Don't miss the opportunity to be part of this exciting event.</p>

<?php
include "footer.inc";
?>
