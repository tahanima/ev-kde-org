<?php
 $page_title = "KDE e.V. supported developer meetings";
 include "header.inc";
?>

<p>For 2007 we plan to intensify our support of focused developer meetings. 
Small gatherings of developers have proven to be a very efficient tool to get 
important work done and strengthen the community. Thanks to the increasing 
number of supporting members the e.V. has the resources to support developer 
meetings financially and in organizational matters. We aim at having 
something like ten KDE e.V. supported developer meetings in 2007 and hope 
this will be particularly useful in this important year where the release of 
KDE 4 is going to happen.</p>

<p>The meetings should be two to three days meetings of around ten developers in
an environment where they can focus on getting work done. The meetings should 
include core people, but also new people to get fresh developers into the 
community and help them to find their way around. At least one or two new 
people should be invited to every meeting.</p>

<p>As the KDE community relies on the continuous growth of generations of
developers, we also want to encourage our newcomers to take part in
the developer gatherings in order to learn and enjoy the culture of KDE in
creating innovative technology.</p>

<p>The e.V. can cover travel and accommodation costs for those who have no
other  means to cover the costs. Participating in developer meeting should be no
personal financial burden to any of the participants. If it simplifies 
logistics and helps developers to concentrate on their work the e.V. can also 
cover food costs. For all expenses the motivation should be to provide a 
productive environment, not to give any special benefits.</p>

<p>The e.V. board can help for example with finding locations or getting flights
booked. It will also provide help and advice where needed and appropriate.</p>

<p>As you know we plan to hire an administrative person for the KDE e.V. One 
important task of this person will be to help to organize the developer 
meetings. This will give us a central point of contact to cope with the 
recurring administrative tasks related to the meetings.</p>

<p>All meetings need to have a person who acts as responsible organizer and 
contact person. If possible this person should be supported by one or two 
co-organizers. The responsibility of the organizer is to coordinate the 
meeting, take care of invitee lists and agenda and interaction with the e.V. 
board.</p>

<p>It is recommended to partner with other organizations or companies to conduct
the meetings. Additional sponsorship is welcome. This can for example happen 
through providing rooms, sending people, taking over costs or similar means.</p>

<p>After the meetings a report has to be written and published on the dot. If 
appropriate it might be accompanied by a private report to the e.V. 
membership. Documentation of the meeting by web or Wiki pages, mails to the 
relevant mailing lists or blogs is highly encouraged.</p>

<p>Some possible topics for developer meetings:
<ul>
<li>KDE Core Libs</li>
<li>KDE Multimedia</li>
<li>Groupware (Akonadi, KDE PIM)</li>
<li>Oxygen, the KDE4 Look</li>
<li>Single Sign On</li>
<li>Development Environment</li>
<li>Office Document Creation</li>
<li>Accessibility</li>
</ul>

<p>A first KDE e.V. supported meeting with the topic KDE PIM/Akonadi has already
happened in January, an ODF meeting is being planned for May.</p>

<p>If you want to help organize a KDE e.V. supported developer meeting please
get in contact with the <a href="mailto:kde-ev-board@kde.org">KDE e.V.
board</a>. We will then sort out what needs to be 
done, what information is required and how to make the meeting happen.</p>

<?php
include "footer.inc";
?>
