---
title: "Spenden und Steuern"
layout: page
---

# Spenden und Steuern

<p>
KDE e.V. ist ein gemeinn&uuml;tziger Verein im Sinne des BGB. KDE e.V. ist laut seiner 
<a href="corporate/statutes-de.html">Satzung</a> dazu verpflichtet, alle Spenden zur 
Unterst&uuml;tzung des KDE-Projektes zu verwenden. 
KDE e.V. ist laut Freistellungsbescheid
des Finanzamtes Darmstadt vom 3. 4. 2009 nach 
&sect; 5 Absatz 1 Nr. 9 KStG von der 
K&ouml;rperschaftssteuer und nach &sect; 3 Nr. 6 GewStG 
von der Gewerbesteuer befreit, da
der Verein gemeinn&uuml;tzigen Zwecken im Sinne der 
&sect;&sect; 51 ff. AO dient.
</p>

<p align="justify">
KDE e.V. f&ouml;rdert die Volks- und Berufsbildung nach Abschnitt A, Nr. 4 der Anlage 1 zu 
&sect; 48 Abs. 2 EStDV und ist daher berechtigt f&uuml;r 
Spenden und Mitgliedsbeitr&auml;ge
Zuwendungsbest&auml;tigungen nach amtlich vorgeschriebenem Vordruck (&sect; 50 Abs. 1 EStDV)
auszustellen.
</p>

<p>
Spenden und Mitgliedsbeitr&auml;ge k&ouml;nnen also steuerlich geltend gemacht werden. Dabei 
sind die folgenden Dinge zu beachten:
</p>
<ul>
    <li>
        F&uuml;r Spenden oder Zuwendungen bis einschlie&szlig;lich 200 &euro;:
        <ul>
            <li>Vermerken Sie auf dem Bareinzahlungsbeleg oder der Buchungsbest&auml;tigung
                 (z.B. Kontoauszug oder Lastschrifteinzugsbeleg) als Betreff im Textfeld
                 das Wort "Spende" <strong>und</strong>
            </li>
            <li>Reichen Sie beim Finanzamt zusammen mit dem Zahlungsbeleg einen Beleg des 
                Vereins ein. Dieser Beleg steht Ihnen 
                <a href="resources/vereinfachter_zuwendungsnachweis_update2014.pdf">hier als PDF-Dokument</a>
                zur Verf&uuml;gung. Sie brauchen ihn nur auszudrucken.
            </li>
        </ul>
    </li>
    <li>
        F&uuml;r Spenden oder Zuwendungen &uuml;ber 200 &euro;:
        <ul>
            <li>
                Vermerken Sie auf dem Zahlungsbeleg neben dem Wort "Spende" auch zus&auml;tzlich 
                Ihre vollst&auml;ndige Adresse.
            </li>
            <li>
                Sie erhalten dann von uns auf Anfrage per Post eine Zuwendungsbest&auml;tigung. 
                Diese Zuwendungsbest&auml;tigung reichen Sie beim Finanzamt ein. Den Zahlungsbeleg 
                brauchen Sie dann nicht mehr einzureichen.
            </li>
        </ul>
    </li>
</ul>

<p>
Wenn sie Fragen haben, nehmen Sie bitte Kontakt mit uns auf. Am einfachsten geht das
 <a href="mailto:kde-ev-board@kde.org">per Email</a> an den Vorstand des KDE e.V.
</p>

<p>
Auf <a href="http://kde.org/community/donations/">unserer Website</a> finden Sie mehr 
Informationen zum Thema "Spenden an den KDE e.V.". Vielen Dank f&uuml;r Ihre 
Unterst&uuml;tzung des KDE Projektes &uuml;ber den KDE e.V.
</p>
