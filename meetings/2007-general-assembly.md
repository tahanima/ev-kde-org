---
title: "General Assembly 2007"
layout: page
---

# General Assembly 2007

<pre>
Dear member of the KDE e.V.,

you are invited to the next general assembly of the KDE e.V. It takes place at 
Graham Hills Building, 50 George Street, Glasgow G1 1QE, Scotland on Monday, 2 
July 2007, starting at 10:00 o'clock am. The entrance to the building is from 
Richmond Street.

The agenda includes the following topics:

1 Welcome by the president of the board
2 Election of a chairman for the general assembly
3 Report of the board
3.1 Report about activities
3.2 Report of the treasurer
3.3 Report of the auditors of accounting
3.4 Relief of the board
4 Report of representatives and working groups of the e.V.
4.1 Report of the representatives to the KDE League
4.2 Report of the representatives to the KDE Free Qt Foundation
4.3 Report of the Marketing Working Group
4.4 Report of the Human Computer Interaction Working Group
4.5 Report of the System Administration Working Group
5 Election of board
5.1 Election of replacement for leaving board member
6 Election of representatives
6.1 Election of representatives to the KDE League
6.2 Election of representatives to the KDE Free Qt Foundation
7 Election of auditors of accounting
8 Vote about change of rules of procedures for supporting members (see 
Appendix A)
9 Vote about Fiduciary License Agreement (see Appendix B)
10 Vote about statement about software patents (see Appendix C)
11 Miscellaneous

Changes and additions to the agenda have to be requested until two weeks 
before the assembly. The final agenda will be announced on Monday, 18 
June 2007. So if you want a topic covered by the general assembly please 
send it to the membership mailing list or the board. The earlier this is done 
the better the topic can be prepared, so please don't wait.

If you are not able to participate in the general assembly in person you can, 
according to section 6.6 of the articles of association of the KDE e.V., ask 
another member to act as proxy for you and execute your voting right. Fill 
out the <a href="http://ev.kde.org/resources/proxy_instructions.pdf">attached
form</a>, sign it and make sure that it's available to the 
chairman of the general assembly on the day of the assembly if you make use 
of this option. Each active member can act as proxy for up to two other 
active members.

If you aren't executing your voting rights personally or through a proxy for 
two consecutive general assemblies your membership status will be changed 
from active to passive, according to section 4.2 of the articles of 
association. Passive and supporting members have the right to attend the 
general assembly, but they have no voting rights.

The general assembly will once again be part of
<a href="http://conference2007.kde.org">aKademy</a>, the biggest KDE 
event world wide, including a conference, a week of coding marathon and BoF 
sessions and the opportunity to meet many of the great people that form the 
KDE community in person. Don't miss the opportunity to be part of this event 
and shape the future of the KDE e.V. by attending the general assembly.

If you need financial help to be able to attend the general assembly you can 
apply for a reimbursement of parts of your travel costs. There is a limited 
budget available for this. See the
<a href="http://ev.kde.org/rules/reimbursement_policy.php">KDE e.V. Travel Cost
Reimbursement Policy</a> for details.

In the name of the board of the KDE e.V.
Cornelius Schumacher
KDE e.V. Vice President

Appendix A

The board asks the membership to remove the US Dollar amounts for the 
membership fees from the rules of procedure for supporting members. The Euro 
amounts remain unchanged. 

Appendix B

The Fiduciary License Agreement (FLA) gives KDE contributors the option to 
give certain rights about the code to the KDE e.V., so that the e.V. can act 
as representative of the community in some legal questions. The text of the 
agreement will be published together with the final agenda for the membership 
assembly. The membership is asked to vote about the text. This doesn't imply 
any transfer or grant of rights. For this contributors have the option to 
individually sign the agreement, once the text has been decided by the 
membership.

Appendix C

As decided on the last membership assembly the e.V. wants to have an official 
statement about software patents, so that it can more effectively advocate 
the interests of the KDE project regarding software patents. A text for the 
statement will be published with the final agenda for the membership 
assembly. The membership is asked to vote about the text.

</pre>

<?php
include "footer.inc";
?>
